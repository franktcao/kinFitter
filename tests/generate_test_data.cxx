#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <cmath>
#include <array>
#include "TLorentzVector.h"
#include "TGenPhaseSpace.h"
#include "Math/Vector4D.h"
#include "ROOT/TDataFrame.hxx"
#include <vector>
#include <tuple>
#include "Math/GenVector/VectorUtil.h"


#ifdef __CLING__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class std::vector<TLorentzVector>+;
#pragma link C++ class std::vector<ROOT::Math::XYZTVector>+;
#endif


/** Generate test data.
 *
 *  Code must be compiled to run: 
 *  root -x generate_test_data.cxx++
 *
 */ 
void generate_test_data(){

  // Seed with a real random value, if available
  std::random_device r;
  // Choose a random mean between 1 and 6
  std::default_random_engine e1(r());
  //auto mean = dist(e1);

  TLorentzVector target(0.0, 0.0, 0.0, 3.727);
  TLorentzVector beam(0.0, 0.0, 6.0, 6.0);
  TLorentzVector W = beam + target;

  //(Momentum, Energy units are Gev/C, GeV)
  Double_t masses[4] = { 3.727, 0.000511, 0.0, 0.0} ;

  auto to_genvector     = [](const TLorentzVector& v) {return ROOT::Math::XYZTVector{v.X(),v.Y(),v.Z(),v.T()};};

  // Use this method to inject random uncertainty
  auto ptr_to_genvector = [&e1](TLorentzVector* v) {
    std::normal_distribution<double> dist(1.0,0.01);
    double delta = dist(e1);
    return ROOT::Math::XYZTVector{delta*v->X(),
                                  delta*v->Y(),
                                  delta*v->Z(),
                                  delta*v->T()};};

  ROOT::Experimental::TDataFrame tdf(100);

  auto tdf_1 = tdf.Define("event", [&]() {
    TGenPhaseSpace event;
    event.SetDecay(W, 4, masses);
    event.Generate();
    std::vector<ROOT::Math::XYZTVector> res;
    res = { ptr_to_genvector(event.GetDecay(0)),
            ptr_to_genvector(event.GetDecay(1)),
            ptr_to_genvector(event.GetDecay(2)),
            ptr_to_genvector(event.GetDecay(3)) };
    return res;
  });

  // And we write out the dataset on disk
  tdf_1.Snapshot("events", "test_data.root");

}

